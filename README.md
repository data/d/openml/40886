# OpenML dataset: Annthyroid

https://www.openml.org/d/40886

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The original thyroid disease (ann-thyroid) dataset from UCI machine learning repository is a classification dataset, which is suited for training ANNs. It has 3772 training instances and 3428 testing instances. It has 15 categorical and 6 real attributes. The problem is to determine whether a patient referred to the clinic is hypothyroid. Therefore three classes are built: normal (not hypothyroid), hyperfunction and subnormal functioning. For outlier detection, both training and testing instances are used, with only 6 real attributes. The hyperfunction and subnormal classes are treated as outlier class and the other one as inliers class. This dataset is not the original dataset. The target variable &quot;Target&quot; is relabeled into &quot;Normal&quot; and &quot;Anomaly&quot; and the categorial variables are deleted.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40886) of an [OpenML dataset](https://www.openml.org/d/40886). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40886/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40886/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40886/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

